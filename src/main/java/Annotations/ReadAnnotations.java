package Annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Read annotations inside the program
 */
public class ReadAnnotations {

    /**
     * Get Value of an annotation
     *
     * @param annotation Annotation to check the value
     * @return Value of the annotation
     */
    private static Object getMethodAnnotation(Annotation annotation) throws InvocationTargetException, IllegalAccessException {
        if (annotation == null) {
            return null;
        }

        Class<? extends Annotation> type = annotation.annotationType();
        Method method = type.getDeclaredMethods()[0];
        return method.invoke(annotation, (Object[]) null);
    }

    /**
     * Check if an annotation is on an interface to take the class that implement it
     *
     * @param interfaceToImplement Interface to check the implementation
     * @return The class that implement the interface
     */
    public static Class<?> ifExistGetClassWhichImplementInterface(Class<?> interfaceToImplement) throws InvocationTargetException, IllegalAccessException {
        Class<?> classImplementedInterface = ReadAnnotations.getDependencyTypeFromClass(interfaceToImplement);

        if (classImplementedInterface != null) {
            return classImplementedInterface;
        }

        return interfaceToImplement;
    }

    /**
     * Get the class to inject in an interface above a field
     *
     * @param field Field to get the class to inject
     * @return The class to inject
     */
    public static Class<?> getDependencyTypeFromField(Field field) throws InvocationTargetException, IllegalAccessException {
        Annotation annotation = field.getAnnotation(DependencyType.class);
        return (Class<?>) getMethodAnnotation(annotation);
    }

    /**
     * Get the class to inject in an interface above a Class
     *
     * @param object Class to get the class to inject
     * @return The class to inject
     */
    public static Class<?> getDependencyTypeFromClass(Class<?> object) throws InvocationTargetException, IllegalAccessException {
        Annotation annotation = object.getAnnotation(DependencyType.class);
        return (Class<?>) getMethodAnnotation(annotation);
    }

    /**
     * Get the class to inject in an interface above a Setter
     *
     * @param method Method to get the class to inject
     * @return The class to inject
     */
    public static Class<?> getDependencyTypeFromMethod(Method method) throws InvocationTargetException, IllegalAccessException {
        Annotation annotation = method.getAnnotation(DependencyType.class);
        return (Class<?>) getMethodAnnotation(annotation);
    }

    /**
     * Get the way to inject the class (setter, constructor, field)
     *
     * @param object The class to inject
     * @return The way to inject
     */
    public static InjectionTypeEnum getInjectionTypeFromClass(Class<?> object) throws InvocationTargetException, IllegalAccessException {
        Annotation annotation = object.getAnnotation(InjectionTypeAnnotation.class);
        Object methodAnnotation = getMethodAnnotation(annotation);
        return (methodAnnotation == null) ? InjectionTypeEnum.FIELD : (InjectionTypeEnum) methodAnnotation;
    }


    /**
     * Check if there is an annotation to inject a class above a Setter
     *
     * @param method Method to get the class to inject
     * @return If there is or not
     */
    public static boolean isDependencyTypeFromMethod(Method method) throws InvocationTargetException, IllegalAccessException {
        Annotation annotation = method.getAnnotation(DependencyType.class);
        Object dependencyInjection = getMethodAnnotation(annotation);
        return dependencyInjection != null;
    }

    /**
     * Check if there is an annotation to inject a class above a Field
     *
     * @param field Field to get the class to inject
     * @return If there is or not
     */
    public static boolean isDependencyTypeFromField(Field field) throws InvocationTargetException, IllegalAccessException {
        Annotation annotation = field.getAnnotation(DependencyType.class);
        Object dependencyInjection = getMethodAnnotation(annotation);
        return dependencyInjection != null;
    }

    /**
     * Check if there is an annotation to get a single instance of the class
     *
     * @param object Object that could be an instance
     * @return If it is or not
     */
    public static boolean isSingleInstance(Class<?> object) throws InvocationTargetException, IllegalAccessException {
        Annotation annotation = object.getAnnotation(InjectionTypeAnnotation.class);
        Object methodAnnotation = getMethodAnnotation(annotation);
        return methodAnnotation != null;
    }

}
