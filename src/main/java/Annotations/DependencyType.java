package Annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Class that implement the interface
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DependencyType {
    Class<?> value();
}
