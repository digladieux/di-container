package Annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Annotation that indicate if we want an instance
 */
@Target(ElementType.TYPE)
public @interface SingleInstance {
}
