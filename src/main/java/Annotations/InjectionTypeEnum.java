package Annotations;

/**
 * Enum that represent the possible injection (Constructor, Field, Setter)
 */
public enum InjectionTypeEnum {
    CONSTRUCTOR, FIELD, SETTER
}
