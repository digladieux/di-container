package Model;

import java.util.*;

/**
 * Class that represent the class to init
 */
public class ImplementationModel {

    /**
     * HashMap that contain the class as the key and the properties to init the class as value
     */
    private HashMap<ClassModel, PropertiesModel[]> hashMap;

    /**
     * Class of ImplementationModel
     *
     * @param hashMap HashMap that contains the data of the file
     */
    public ImplementationModel(HashMap<ClassModel, PropertiesModel[]> hashMap) {
        this.hashMap = hashMap;
    }

    /**
     * Get the value regarding the class
     *
     * @param search The class that you are looking for
     * @return All the properties of the class
     */
    public PropertiesModel[] getPropertiesByType(Class<?> search) {
        List<PropertiesModel> propertiesModels = new ArrayList<>();
        for (Map.Entry<ClassModel, PropertiesModel[]> entry : hashMap.entrySet()) {
            if (entry.getKey().className == search) {
                PropertiesModel[] propertiesModel = entry.getValue();
                Collections.addAll(propertiesModels, propertiesModel);
                break;
            }
        }
        return propertiesModels.toArray(new PropertiesModel[0]);
    }

    /**
     * Is the class inside the hashMap
     *
     * @param search Class that you are looking for
     * @return Is inside or not
     */
    public boolean containClassByType(Class<?> search) {
        for (ClassModel classModel : hashMap.keySet()) {
            if (classModel.className == search) {
                return true;
            }
        }
        return false;
    }
}
