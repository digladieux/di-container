package Model;

/**
 * Properties to initialize a class
 */
public class PropertiesModel {

    /**
     * Name of the class
     */
    public String name;

    /**
     * Value of the class (if available)
     */
    public String value;

    /**
     * Id of the class to init, could be implemented by multiple class (if available)
     */
    public String[] refs;

    /**
     * Constructor of PropertiesModel
     *
     * @param name  Name of the class
     * @param value Value of the class
     * @param refs  Id of the class to init, could be implemented by multiple class
     */
    public PropertiesModel(String name, String value, String[] refs) {
        this.name = name;
        this.value = value;
        this.refs = refs;
    }

}
