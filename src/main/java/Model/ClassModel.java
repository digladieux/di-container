package Model;

/**
 * Key to identity a class inside the data file
 */
public class ClassModel {

    /**
     * Id of the class
     */
    public String id;

    /**
     * Name of the class
     */
    public Class<?> className;

    /**
     * Constructor of ClassModel
     *
     * @param id        Id of the class
     * @param className Name of the class
     */
    public ClassModel(String id, Class<?> className) {
        this.id = id;
        this.className = className;
    }
}
