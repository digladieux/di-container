package Parser;

import Model.ClassModel;
import Model.PropertiesModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

/**
 * Convert Json to Model
 */
public class JsonToImplementation {

    public HashMap<ClassModel, PropertiesModel[]> getImplementation(JsonDeserializeModel deserializeModel) throws ClassNotFoundException, InvalidPropertiesFormatException {
        HashMap<ClassModel, PropertiesModel[]> hashMap = new HashMap<>();
        for (JsonClassModel jsonClassModel : deserializeModel.beans) {

            List<PropertiesModel> properties = new ArrayList<>();
            for (JsonProperties jsonProperties : jsonClassModel.properties) {
                if(jsonProperties.refs == null || isIdsValid(jsonProperties.refs, deserializeModel.beans)) {
                    properties.add(new PropertiesModel(jsonProperties.name, jsonProperties.value, jsonProperties.refs));
                    Class<?> className = Class.forName(jsonClassModel.class_name);
                    ClassModel classModel = new ClassModel(jsonClassModel.id, className);
                    hashMap.put(classModel, properties.toArray(new PropertiesModel[0]));
                } else {
                    throw new InvalidPropertiesFormatException("Invalid Refs");
                }

            }
        }

        return hashMap;
    }

    /**
     * Check if all the ids are valid and present inside the file
     *
     * @param refs            List of refs to check
     * @param jsonClassModels All the class present inside the file
     * @return All the ids are correct or no
     */
    private boolean isIdsValid(String[] refs, JsonClassModel[] jsonClassModels) {
        for (String ref : refs) {
            if (!isIdValid(ref, jsonClassModels)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if one id is correct
     *
     * @param ref             Ref to check
     * @param jsonClassModels All the class present inside the file
     * @return The id is correct or no
     */
    private boolean isIdValid(String ref, JsonClassModel[] jsonClassModels) {
        for (JsonClassModel jsonClassModel : jsonClassModels) {
            if (jsonClassModel.id.equals(ref)) {
                return true;
            }
        }
        return false;
    }
}
