package Parser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

/**
 * Properties to init a class
 */
public class JsonProperties {

    /**
     * Name of the property
     */
    @JsonProperty("name")
    public String name;

    /**
     * Value of the property
     */
    @JsonProperty("value")
    public String value;

    /**
     * Ids of the class to initialize
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("refs")
    public String[] refs;
}
