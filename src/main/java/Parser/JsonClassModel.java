package Parser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


/**
 * Json that represent the class
 */
@JsonDeserialize()
public class JsonClassModel {

    /**
     * Id of the class
     */
    @JsonProperty("id")
    public String id;

    /**
     * Name of the class
     */
    @JsonProperty("class")
    public String class_name;

    /**
     * All the properties to init
     */
    @JsonProperty("properties")
    public JsonProperties[] properties;

}
