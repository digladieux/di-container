package Parser;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Json that represent the list of the class
 */
public class JsonDeserializeModel {

    /**
     * List of the class
     */
    @JsonProperty("beans")
    public JsonClassModel[] beans;
}