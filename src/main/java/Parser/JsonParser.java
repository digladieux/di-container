package Parser;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Class to deserialize the data from the file
 *
 * @param <T> Type of the class you want to parse
 */
public class JsonParser<T> {

    /**
     * File that contain the config
     */
    private File file;

    /**
     * Constructor of JsonParser
     *
     * @param file File
     */
    public JsonParser(File file) {
        this.file = file;

    }

    /**
     * Deserialize the data
     * @param classToPopulate Class you want to deserialize
     * @return The class deserialized
     * @throws IOException File Invalid, the class is not represented inside the file
     */
    public T deserialize(Class<T> classToPopulate) throws IOException {

        //read json file data to String
        byte[] jsonData = Files.readAllBytes(this.file.toPath());

        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();

        //convert json string to object
        return objectMapper.readValue(jsonData, classToPopulate);


    }
}
