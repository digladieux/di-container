package Container;

import Annotations.ReadAnnotations;
import Model.ImplementationModel;
import Model.PropertiesModel;

import java.io.InvalidClassException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;


/**
 * Container to inject via the setters
 */
class ContainerSetterInjection {

    /**
     * Contain the information about how to implement the classes
     */
    private ImplementationModel implementationsModel;

    /**
     * Constructor of ContainerSetterInjection
     *
     * @param implementationModel The information to implement the classes
     */
    public ContainerSetterInjection(ImplementationModel implementationModel) {
        this.implementationsModel = implementationModel;
    }

    /**
     * Create an object from the class
     *
     * @param objectToCreate Class to initialize
     * @return The object initialized
     */
    public Object create(Class<?> objectToCreate) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, InvalidClassException, InvalidPropertiesFormatException {

        Constructor<?> constructor = objectToCreate.getConstructor();
        Object instance = constructor.newInstance();

        // Does the class is inside the config file
        if (this.implementationsModel.containClassByType(objectToCreate)) {
            this.initWithDataFile(instance);
        } else {
            this.initWithoutDataFile(instance);
        }
        return instance;
    }

    /**
     * Get the right properties regarding the setter
     *
     * @param propertiesModels List of properties
     * @param setterName       Name of the setter
     * @return The property necessary to init the setter
     */
    private PropertiesModel getPropertyRegardingSetter(PropertiesModel[] propertiesModels, String setterName) {
        for (PropertiesModel propertiesModel : propertiesModels) {
            if (propertiesModel.name.equals(setterName)) {
                return propertiesModel;
            }
        }
        return null;
    }

    /**
     * Get all the setters of the class
     *
     * @param objectToCreate Class that contain the setter
     * @return Lists of the setters
     */
    private Method[] getSetters(Class<?> objectToCreate) {
        Method[] methods = objectToCreate.getDeclaredMethods();
        List<Method> setters = new ArrayList<>();
        for (Method method : methods) {
            if (method.getName().startsWith("set")) {
                setters.add(method);
            }
        }
        return setters.toArray(new Method[0]);
    }

    /**
     * Initialize an object which is inside the datafile
     *
     * @param instance Object to initialize
     */
    private void initWithDataFile(Object instance) throws InvocationTargetException, IllegalAccessException, InvalidPropertiesFormatException {
        PropertiesModel[] propertiesModels = this.implementationsModel.getPropertiesByType(instance.getClass());
        Method[] setters = this.getSetters(instance.getClass());

        // If there is not enough setters or properties to init all the field, throw error
        if (setters.length != propertiesModels.length) {
            throw new InvalidPropertiesFormatException("You need to implement all the setters");
        }
        for (Method setter : setters) {
            String setterName = setter.getName().substring(3);
            setterName = setterName.substring(0, 1).toLowerCase() + setterName.substring(1);
            PropertiesModel propertiesModel = this.getPropertyRegardingSetter(propertiesModels, setterName);
            Object value;

            // The name of the setter is maybe invalid because the id is not inside the datafile
            if (propertiesModel == null) {
                throw new InvalidPropertiesFormatException("The name of the argument in setter is invalid");
            }

            // Check if the property needs to be initialize or not
            else {
                value = propertiesModel.value;
            }

            setter.invoke(instance, value);
        }
    }

    private void initWithoutDataFile(Object instance) throws InvalidPropertiesFormatException, InvocationTargetException, IllegalAccessException, InvalidClassException, InstantiationException, NoSuchMethodException {
        Method[] setters = getSetters(instance.getClass());
        int fieldsLength = instance.getClass().getDeclaredFields().length;

        // Check if there is enough setter to init all the fields
        if (setters.length != fieldsLength) {
            throw new InvalidPropertiesFormatException("You need to implement all the setters") ;
        }

        for (Method setter : setters) {
            Object setterValue ;
            Class<?> setterType = setter.getParameterTypes()[0];

            // Check if the parameter is inside the file
            if (this.implementationsModel.containClassByType(setterType)) {
                if (ReadAnnotations.isDependencyTypeFromMethod(setter)) {
                    setterType = ReadAnnotations.getDependencyTypeFromMethod(setter);
                }
                setterValue = this.create(setterType);
            }

            // TODO : parameters getName not working
            /*else if (this.implementationsModel.containClassById(setterType.getName())){
                setterValue = this.implementationsModel.getPropertiesById(setterType.getName());
            } */
            else {
                setterValue = this.create(setter.getParameterTypes()[0]);
            }
            setter.invoke(instance, setterValue );
        }
    }

}
