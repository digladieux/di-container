package Container;

import Annotations.ReadAnnotations;
import Model.ImplementationModel;
import Model.PropertiesModel;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.InvalidPropertiesFormatException;


/**
 * Container to inject via the fields
 */
class ContainerFieldInjection {

    /**
     * Contain the information about how to implement the classes
     */
    private ImplementationModel implementationsModel;

    /**
     * Constructor of ContainerFieldInjection
     *
     * @param implementationModel The information to implement the classes
     */
    public ContainerFieldInjection(ImplementationModel implementationModel) {
        this.implementationsModel = implementationModel;
    }

    /**
     * Create an object from the class
     *
     * @param objectToCreate Class to initialize
     * @return The object initialized
     */
    public Object create(Class<?> objectToCreate) throws NoSuchMethodException, InvalidPropertiesFormatException, IllegalAccessException, InvocationTargetException, InstantiationException {

        // Init the object with the default constructor
        Field[] fields = objectToCreate.getDeclaredFields();
        Constructor<?> constructor = objectToCreate.getConstructor();
        Object instance = constructor.newInstance();
        PropertiesModel[] propertiesModels = null;

        // The class is inside the datafile
        if (this.implementationsModel.containClassByType(objectToCreate)) {
            propertiesModels = this.implementationsModel.getPropertiesByType(objectToCreate) ;
        }
        for(Field field : fields) {
            Class<?> dependencyField;
            Object initField;

            // If the field has a dependency (like an interface)
            if (ReadAnnotations.isDependencyTypeFromField(field)) {
                dependencyField = ReadAnnotations.getDependencyTypeFromField(field);
                if (this.implementationsModel.containClassByType(dependencyField)) {
                    initField = this.create(dependencyField);
                } else {
                    throw new InvalidPropertiesFormatException("The dependency in the field is not inside the JSON file");
                }
            } else if (propertiesModels != null) {
                initField = this.researchValue(field, propertiesModels);
            } else {
                initField = this.create(field.getType());
            }

            if (initField == null) {
                throw new InvalidPropertiesFormatException("The initialisation of the argument return null");
            }
            field.setAccessible(true);
            field.set(instance, initField);
            field.setAccessible(false);
        }


        return instance;
    }

    /**
     * Return the value to put in the field
     *
     * @param field            Field to init
     * @param propertiesModels All the properties
     * @return The object init
     */
    private Object researchValue(Field field, PropertiesModel[] propertiesModels) {
        for (PropertiesModel propertiesModel : propertiesModels) {
            if (propertiesModel.name.equals(field.getName()) && propertiesModel.value != null) {
                return propertiesModel.value;
            }
        }
        return null;
    }


}
