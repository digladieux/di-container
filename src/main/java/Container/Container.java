package Container;

import Annotations.InjectionTypeEnum;
import Annotations.ReadAnnotations;
import Model.ClassModel;
import Model.ImplementationModel;
import Model.PropertiesModel;
import Parser.JsonDeserializeModel;
import Parser.JsonParser;
import Parser.JsonToImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InvalidClassException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;

import static Annotations.ReadAnnotations.ifExistGetClassWhichImplementInterface;

/**
 * Container to inject data
 */
public class Container {

    /**
     * Constructor Injector
     */
    private ContainerConstructorInjection containerConstructorInjection;

    /**
     * Setter Injection
     */
    private ContainerSetterInjection containerSetterInjection;

    /**
     * Field Injector
     */
    private ContainerFieldInjection containerFieldInjection;

    /**
     * Logger
     */
    final Logger logger = LoggerFactory.getLogger(JsonParser.class);
    /**
     * Class that contain their initialization
     */
    private HashMap<Class<?>, Object> instanceClass;

    /**
     * Constructor of Container
     *
     * @param file File where is the config
     * @throws IOException            General errors
     * @throws ClassNotFoundException A class inside the config don't exist
     */
    public Container(File file) throws IOException, ClassNotFoundException {
        JsonParser<JsonDeserializeModel> jsonParser = new JsonParser<>(file);
        JsonDeserializeModel jsonDeserializeModel = jsonParser.deserialize(JsonDeserializeModel.class);
        JsonToImplementation jsonToImplementation = new JsonToImplementation();
        HashMap<ClassModel, PropertiesModel[]> hashMap = jsonToImplementation.getImplementation(jsonDeserializeModel);

        ImplementationModel implementationsModel = new ImplementationModel(hashMap);

        this.containerConstructorInjection = new ContainerConstructorInjection(implementationsModel);
        this.containerSetterInjection = new ContainerSetterInjection(implementationsModel);
        this.containerFieldInjection = new ContainerFieldInjection(implementationsModel);
        this.instanceClass = new HashMap<>();
        logger.info("Container Initialized");
    }

    /**
     * Creation of the object we want
     *
     * @param objectToCreate The class you want to initialize
     * @return The object created
     */
    public Object create(Class<?> objectToCreate) throws InvocationTargetException, IllegalAccessException, InstantiationException, InvalidPropertiesFormatException, InvalidClassException, NoSuchMethodException {

        objectToCreate = ifExistGetClassWhichImplementInterface(objectToCreate);
        if (ReadAnnotations.isSingleInstance(objectToCreate) && this.instanceClass.containsKey(objectToCreate)) {
            return this.instanceClass.get(objectToCreate);
        }

        Object instanceObject = null;

        InjectionTypeEnum injectionTypeEnum = ReadAnnotations.getInjectionTypeFromClass(objectToCreate);
        switch (injectionTypeEnum) {
            case CONSTRUCTOR:
                instanceObject = this.containerConstructorInjection.create(objectToCreate);
                break;
            case SETTER:
                instanceObject = this.containerSetterInjection.create(objectToCreate);
                break;
            case FIELD:
                instanceObject = this.containerFieldInjection.create(objectToCreate);
                break;
        }

        if (ReadAnnotations.isSingleInstance(objectToCreate) && !this.instanceClass.containsKey(objectToCreate)) {
            this.instanceClass.put(objectToCreate, instanceObject);
        }
        return instanceObject;
    }
}
