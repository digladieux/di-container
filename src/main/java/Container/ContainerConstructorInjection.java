package Container;

import Annotations.ReadAnnotations;
import Model.ImplementationModel;
import Model.PropertiesModel;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

/**
 * Container to inject via the constructor
 */
class ContainerConstructorInjection {

    /**
     * Contain the information about how to implement the classes
     */
    private ImplementationModel implementationsModel;

    /**
     * Constructor of ContainerConstructorInjection
     *
     * @param implementationModel The information to implement the classes
     */
    public ContainerConstructorInjection(ImplementationModel implementationModel) {
        this.implementationsModel = implementationModel;
    }

    /**
     * Create an object from the class
     *
     * @param objectToCreate Class to initialize
     * @return The object initialized
     */
    public Object create(Class<?> objectToCreate) throws InvalidPropertiesFormatException, IllegalAccessException, InstantiationException, InvocationTargetException {

        // Get the longest constructor, because we inject by constructor, so we need all the parameters
        Constructor<?> constructor = this.getLongestConstructor(objectToCreate);
        int fieldsLength = objectToCreate.getDeclaredFields().length;

        // We check if all the fields could be initialize with this constructor. If not, error
        if (constructor == null || fieldsLength != constructor.getParameterCount()) {
            throw new InvalidPropertiesFormatException("You need a constructor to init all the field");
        }

        // Does the class is inside the config file
        if (this.implementationsModel.containClassByType(objectToCreate)) {
            return this.initWithDataFile(objectToCreate, constructor);
        } else {
            return this.initWithoutDataFile(objectToCreate, constructor);
        }
    }

    /**
     * Initialize the object if it is inside the data file
     *
     * @param objectToCreate The object to create
     * @param constructor    The constructor to use
     * @return The object Initialise
     */
    private Object initWithDataFile(Class<?> objectToCreate, Constructor<?> constructor) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        PropertiesModel[] propertiesModels = this.implementationsModel.getPropertiesByType(objectToCreate);

        List<Object> initProperties = new ArrayList<>();
        Field[] fields = objectToCreate.getDeclaredFields();
        for (PropertiesModel propertiesModel : propertiesModels) {
            initProperties.add(propertiesModel.value);
        }

        return constructor.newInstance(initProperties.toArray());
    }

    /**
     * Initialize the object which is not inside the datafile
     *
     * @param objectToCreate Object to initialize
     * @param constructor    The constructor to use
     * @return The object initialized
     */
    private Object initWithoutDataFile(Class<?> objectToCreate, Constructor<?> constructor) throws InvocationTargetException, InvalidPropertiesFormatException, InstantiationException, IllegalAccessException {
        ArrayList<Object> objects = new ArrayList<>();
        for (Field field : objectToCreate.getDeclaredFields()) {
            Class<?> classToInit;
            if (ReadAnnotations.isDependencyTypeFromField(field)) {
                classToInit = ReadAnnotations.getDependencyTypeFromField(field);
            } else {
                classToInit = field.getType();
            }
            objects.add(this.create(classToInit));
        }
        return constructor.newInstance(objects.toArray());

    }

    /**
     * Get the longest constructor of the class to init all the fields
     *
     * @param object The class you want to know the longest constructor
     * @return The constructor
     */
    private Constructor<?> getLongestConstructor(Class<?> object) {
        Constructor<?>[] constructors = object.getDeclaredConstructors();
        Constructor<?> rightConstructor = null;
        for (Constructor<?> constructor : constructors) {
            if (rightConstructor == null || constructor.getParameterTypes().length > rightConstructor.getParameterTypes().length) {
                rightConstructor = constructor;
            }
        }
        return rightConstructor;
    }
}
