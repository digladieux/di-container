package SetterInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class CSetterInjection {

    private ASetterInjection aSetterInjection;

    public CSetterInjection() {
    }

    public void setASetterInjection(ASetterInjection aSetterInjection) {
        this.aSetterInjection = aSetterInjection;
    }

    @Override
    public String toString() {
        return this.aSetterInjection.toString();
    }

}
