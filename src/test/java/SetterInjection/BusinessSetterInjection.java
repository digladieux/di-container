package SetterInjection;

import Annotations.DependencyType;
import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class BusinessSetterInjection {

    private ISetterInjection setterInjectionFirst;

    private ISetterInjection setterInjectionSecond;

    private CSetterInjection cSetterInjection;

    public BusinessSetterInjection() {
    }

    @DependencyType(ASetterInjection.class)
    public void setSetterInjectionFirst(ISetterInjection setterInjectionFirst) {
        this.setterInjectionFirst = setterInjectionFirst;
    }

    @DependencyType(BSetterInjection.class)
    public void setSetterInjectionSecond(ISetterInjection setterInjectionSecond) {
        this.setterInjectionSecond = setterInjectionSecond;
    }

    public void setCSetterInjection(CSetterInjection cSetterInjection) {
        this.cSetterInjection = cSetterInjection;
    }

    @Override
    public String toString() {
        return this.setterInjectionFirst.toString() + "\n" + this.setterInjectionSecond.toString() + "\n" + this.cSetterInjection.toString();
    }
}
