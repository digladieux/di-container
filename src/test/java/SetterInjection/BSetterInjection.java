package SetterInjection;

import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class BSetterInjection implements ISetterInjection {

    private String value ;

    public BSetterInjection() {}

    public void setValue(String value) {
        this.value = value ;
    }
    @Override
    public String toString() {
        return this.value;
    }

}
