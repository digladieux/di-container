package SetterInjection;

import Annotations.DependencyType;
import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
@DependencyType(ASetterInjection.class)
public interface ISetterInjection {
    @Override
    String toString() ;

}
