package SetterInjection;

import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class ASetterInjection implements ISetterInjection {

    private String value ;

    public ASetterInjection() {}

    public void setValue(String value) {
        this.value = value ;
    }
    @Override
    public String toString() {
        return this.value;
    }
}
