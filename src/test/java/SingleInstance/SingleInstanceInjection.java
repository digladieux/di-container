package SingleInstance;

import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;
import Annotations.SingleInstance;

@InjectionTypeAnnotation(InjectionTypeEnum.CONSTRUCTOR)
@SingleInstance
public class SingleInstanceInjection {

    private String value;

    public SingleInstanceInjection(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
