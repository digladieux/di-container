package ConstructorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.CONSTRUCTOR)
public class CConstructorInjection {

    private AConstructorInjection aConstructorInjection;

    public CConstructorInjection(AConstructorInjection aConstructorInjection) {
        this.aConstructorInjection = aConstructorInjection;
    }

    @Override
    public String toString() {
        return this.aConstructorInjection.toString();
    }

}
