package ConstructorInjection;

import Annotations.DependencyType;
import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.CONSTRUCTOR)
public class BusinessConstructorInjection {

    @DependencyType(AConstructorInjection.class)
    private IConstructorInjection constructorInjectionFirst;

    @DependencyType(BConstructorInjection.class)
    private IConstructorInjection constructorInjectionSecond;

    private CConstructorInjection cConstructorInjection;

    public BusinessConstructorInjection(IConstructorInjection constructorInjectionFirst, IConstructorInjection constructorInjectionSecond, CConstructorInjection cConstructorInjection) {
        this.constructorInjectionFirst = constructorInjectionFirst;
        this.constructorInjectionSecond = constructorInjectionSecond;
        this.cConstructorInjection = cConstructorInjection;
    }

    @Override
    public String toString() {
        return this.constructorInjectionFirst.toString() + "\n" + this.constructorInjectionSecond.toString() + "\n" + this.cConstructorInjection.toString();
    }
}
