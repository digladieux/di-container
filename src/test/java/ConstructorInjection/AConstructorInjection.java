package ConstructorInjection;

import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.CONSTRUCTOR)
public class AConstructorInjection implements IConstructorInjection {

    private String value ;

    public AConstructorInjection(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
