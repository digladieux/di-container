package ConstructorInjection;

import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.CONSTRUCTOR)
public class BConstructorInjection implements IConstructorInjection {

    private String value ;

    public BConstructorInjection(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

}
