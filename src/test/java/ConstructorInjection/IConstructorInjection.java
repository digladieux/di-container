package ConstructorInjection;

import Annotations.DependencyType;
import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.CONSTRUCTOR)
@DependencyType(AConstructorInjection.class)
public interface IConstructorInjection {
    @Override
    String toString() ;

}
