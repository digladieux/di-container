package Test;

import Container.Container;
import FieldInjection.AFieldInjection;
import FieldInjection.BusinessFieldInjection;
import FieldInjection.CFieldInjection;
import FieldInjection.IFieldInjection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ContainerFieldInjectionTest {
    private Container container = getContainer();

    @Test
    void createFromField_InsideDataFile_AFieldInjection() {
        AFieldInjection aFieldInjection = null;
        try {
            aFieldInjection = (AFieldInjection) container.create(AFieldInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, aFieldInjection);
        Assertions.assertEquals(aFieldInjection.toString(), StaticValue.AField);
    }

    @Test
    void createFromField_InsideDataFile_IFieldInjection() {
        IFieldInjection fieldInjection = null;
        try {
            fieldInjection = (IFieldInjection) container.create(IFieldInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, fieldInjection);
        Assertions.assertEquals(fieldInjection.toString(), StaticValue.AField);
    }

    @Test
    void createFromField_OutsideDataFile_CFieldInjection() {
        CFieldInjection cFieldInjection = null;
        try {
            cFieldInjection = (CFieldInjection) container.create(CFieldInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, cFieldInjection);
        Assertions.assertEquals(cFieldInjection.toString(), StaticValue.AField);
    }


    @Test
    void createFromField_OutsideDataFile_BusinessFieldInjection() {
        BusinessFieldInjection businessFieldInjection = null;
        try {
            businessFieldInjection = (BusinessFieldInjection) container.create(BusinessFieldInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, businessFieldInjection);
        assertEquals(businessFieldInjection.toString(), StaticValue.AField + "\n" + StaticValue.BField + "\n" + StaticValue.AField);
    }

    private Container getContainer() {
        Container container;
        try {
            String fileName = "../field_injection.json";
            File file = new File(getClass().getResource(fileName).toURI());
            container = new Container(file);
        } catch (IOException | ClassNotFoundException | URISyntaxException e) {
            assert false;
            return null;
        }
        return container;
    }
}
