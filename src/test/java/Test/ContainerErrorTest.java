package Test;

import Container.Container;
import ErrorInjection.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class ContainerErrorTest {
    private Container container = getContainer();

    @Test
    void createFromSetter_Throw_InvalidNumberSetterInsideFile() {
        try {
            container.create(InvalidNumberSetterInsideFile.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }


    @Test
    void createFromSetter_Throw_InvalidNameSetterInsideFile() {
        try {
            container.create(InvalidNameSetterInsideFile.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    void createFromSetter_Throw_InvalidNumberPropertiesSetterInsideFile() {
        try {
            container.create(InvalidNumberPropertiesSetterInsideFile.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    void createFromSetter_Throw_InvalidNameSetterOutsideFile() {
        try {
            container.create(InvalidNameSetterOutsideFile.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    void createFromSetter_Throw_InvalidNumberSetterOutsideFile() {
        try {
            container.create(InvalidNumberSetterOutsideFile.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    void createFromConstructor_Throw_InvalidNumberArgumentsConstructor() {
        try {
            container.create(InvalidNumberArgumentsConstructor.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    void createFromField_Throw_InvalidNameDependencyFieldInsideFile() {
        try {
            container.create(InvalidNameDependencyFieldInsideFile.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    void createFromField_Throw_InvalidNamePropertiesFieldInsideFile() {
        try {
            container.create(InvalidNamePropertiesFieldInsideFile.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    void createFromSetter_Throw_InvalidNumberArgumentsSetter() {
        try {
            container.create(InvalidNumberArgumentsSetter.class);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    private Container getContainer() {
        Container container;
        try {
            String fileName = "../error_injection.json";
            File file = new File(getClass().getResource(fileName).toURI());
            container = new Container(file);
        } catch (IOException | ClassNotFoundException | URISyntaxException e) {
            assert false;
            return null;
        }
        return container;
    }


}
