package Test;

import Container.Container;
import SingleInstance.SingleInstanceInjection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ContainerSingleInstanceTest {

    private Container container = getContainer();

    @Test
    void SingleInstance_NotInit() {
        SingleInstanceInjection singleInstanceInjection = null;
        try {
            container.create(SingleInstanceInjection.class);
            singleInstanceInjection = (SingleInstanceInjection) container.create(SingleInstanceInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, singleInstanceInjection);
        Assertions.assertEquals(singleInstanceInjection.toString(), StaticValue.StaticInstance);
    }

    private Container getContainer() {
        Container container;
        try {
            String fileName = "../static_injection.json";
            File file = new File(getClass().getResource(fileName).toURI());
            container = new Container(file);
        } catch (IOException | ClassNotFoundException | URISyntaxException e) {
            assert false;
            return null;
        }
        return container;
    }
}
