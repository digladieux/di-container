package Test;

import Parser.JsonDeserializeModel;
import Parser.JsonParser;
import Parser.JsonToImplementation;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.InvalidPropertiesFormatException;

public class JsonToImplementationTest {

    @Test
    public void InvalidClassName() {
        JsonDeserializeModel jsonDeserializeModel = null;
        try {
            String invalidClassNameFileName = "../invalid_class_name.json";
            File file = new File(getClass().getResource(invalidClassNameFileName).toURI());
            JsonParser<JsonDeserializeModel> jsonParser = new JsonParser<>(file);
            jsonDeserializeModel = jsonParser.deserialize(JsonDeserializeModel.class);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        assert jsonDeserializeModel != null;

        JsonToImplementation jsonToImplementation = new JsonToImplementation();

        try {
            jsonToImplementation.getImplementation(jsonDeserializeModel);
            assert false;
        } catch (ClassNotFoundException | InvalidPropertiesFormatException e) {
            assert true ;
        }
    }

    @Test
    public void InvalidRefs() {
        JsonDeserializeModel jsonDeserializeModel = null;
        try {
            String invalidRefsFileName = "../invalid_refs.json";
            File file = new File(getClass().getResource(invalidRefsFileName).toURI());
            JsonParser<JsonDeserializeModel> jsonParser = new JsonParser<>(file);

            jsonDeserializeModel = jsonParser.deserialize(JsonDeserializeModel.class);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        assert jsonDeserializeModel != null;

        JsonToImplementation jsonToImplementation = new JsonToImplementation();

        try {
            jsonToImplementation.getImplementation(jsonDeserializeModel);
            assert false;
        } catch (ClassNotFoundException | InvalidPropertiesFormatException e) {
            assert true ;
        }
    }

    @Test
    public void ValidImplementation() {
        JsonDeserializeModel jsonDeserializeModel = null;
        try {
            String validFileName = "../constructor_injection.json";
            File file = new File(getClass().getResource(validFileName).toURI());
            JsonParser<JsonDeserializeModel> jsonParser = new JsonParser<>(file);
            jsonDeserializeModel = jsonParser.deserialize(JsonDeserializeModel.class);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        assert jsonDeserializeModel != null;

        JsonToImplementation jsonToImplementation = new JsonToImplementation();

        try {
            jsonToImplementation.getImplementation(jsonDeserializeModel);
            assert true;
        } catch (ClassNotFoundException | InvalidPropertiesFormatException e) {
            e.printStackTrace();
            assert false ;
        }
    }
}
