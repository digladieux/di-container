package Test;

import ConstructorInjection.BusinessConstructorInjection;
import Parser.JsonDeserializeModel;
import Parser.JsonParser;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class JsonParserTest {
    final Logger logger = LoggerFactory.getLogger(JsonParserTest.class);
    private String filePath = "../constructor_injection.json";

    @Test
    public void FileInvalid() {
        URL uri = getClass().getResource("../toto.json");
        assert (uri == null);

    }

    @Test
    public void XMLParserPossible() {
        try {
            File file = new File(getClass().getResource(filePath).toURI());
            JsonParser<JsonDeserializeModel> jsonParser = new JsonParser<>(file);
            jsonParser.deserialize(JsonDeserializeModel.class);
            assert true;

        } catch (IOException | URISyntaxException e) {
            logger.info(e.getMessage());
            assert false;
        }
    }

    @Test
    public void XMLParserImpossible() {
        try {
            File file = new File(getClass().getResource(filePath).toURI());
            JsonParser<BusinessConstructorInjection> jsonParser = new JsonParser<>(file);
            jsonParser.deserialize(BusinessConstructorInjection.class);
            assert false;

        } catch (IOException | URISyntaxException e) {
            logger.info(e.getMessage());
            assert true;
        }
    }

    @Test
    public void CheckValueDataXML() {
        JsonDeserializeModel jsonDeserializeModel = null ;

        try {
            File file = new File(getClass().getResource(filePath).toURI());
            JsonParser<JsonDeserializeModel> jsonParser = new JsonParser<>(file);
            jsonDeserializeModel = jsonParser.deserialize(JsonDeserializeModel.class);
            assert true;

        } catch (IOException | URISyntaxException e) {
            logger.info(e.getMessage());
            assert false;
        }

        assert jsonDeserializeModel != null;
        assert (jsonDeserializeModel.beans.length == 3);
        assert (jsonDeserializeModel.beans[0].id.equals("constructorInjection"));
        assert (jsonDeserializeModel.beans[0].class_name.equals("ConstructorInjection.IConstructorInjection"));
        assert (jsonDeserializeModel.beans[0].properties[0].name.equals("constructorInjection"));
        assert (jsonDeserializeModel.beans[0].properties[0].value == null);
        assert (jsonDeserializeModel.beans[0].properties[0].refs[0].equals("constructorInjectionFirst"));
        assert (jsonDeserializeModel.beans[0].properties[0].refs[1].equals("constructorInjectionSecond"));

        assert (jsonDeserializeModel.beans[1].id.equals("constructorInjectionFirst"));
        assert (jsonDeserializeModel.beans[1].class_name.equals("ConstructorInjection.AConstructorInjection"));
        assert (jsonDeserializeModel.beans[1].properties[0].name.equals("value"));
        assert (jsonDeserializeModel.beans[1].properties[0].value.equals("A Constructor"));
        assert (jsonDeserializeModel.beans[1].properties[0].refs == null);

        assert (jsonDeserializeModel.beans[2].id.equals("constructorInjectionSecond"));
        assert (jsonDeserializeModel.beans[2].class_name.equals("ConstructorInjection.BConstructorInjection"));
        assert (jsonDeserializeModel.beans[2].properties[0].name.equals("value"));
        assert (jsonDeserializeModel.beans[2].properties[0].value.equals("B Constructor"));
        assert (jsonDeserializeModel.beans[2].properties[0].refs == null);


    }
}
