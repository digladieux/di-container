package Test;

public final class StaticValue {
    public static String AConstructor = "A Constructor";
    public static String BConstructor = "B Constructor";
    public static String DConstructor = "D Constructor";
    public static String AField = "A Field";
    public static String BField = "B Field";
    public static String ASetter = "A Setter";
    public static String BSetter = "B Setter";
    public static String DSetter = "D Setter";

    public static String StaticInstance = "Static Instance";
}

