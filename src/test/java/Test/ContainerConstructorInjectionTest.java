package Test;

import ConstructorInjection.AConstructorInjection;
import ConstructorInjection.BusinessConstructorInjection;
import ConstructorInjection.CConstructorInjection;
import ConstructorInjection.IConstructorInjection;
import Container.Container;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ContainerConstructorInjectionTest {
    private Container container = getContainer();

    @Test
    void createFromConstructor_InsideDataFile_AConstructorInjection() {
        AConstructorInjection aConstructorInjection = null;
        try {
            aConstructorInjection = (AConstructorInjection) container.create(AConstructorInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, aConstructorInjection);
        Assertions.assertEquals(aConstructorInjection.toString(), StaticValue.AConstructor);
    }

    @Test
    void createFromConstructor_InsideDataFile_IConstructorInjection() {
        IConstructorInjection constructorInjection = null;
        try {
            constructorInjection = (AConstructorInjection) container.create(IConstructorInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, constructorInjection);
        Assertions.assertEquals(constructorInjection.toString(), StaticValue.AConstructor);
    }


    @Test
    void createFromConstructor_OutsideDataFile_CConstructorInjection() {
        CConstructorInjection cConstructorInjection = null;
        try {
            cConstructorInjection = (CConstructorInjection) container.create(CConstructorInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, cConstructorInjection);
        Assertions.assertEquals(cConstructorInjection.toString(), StaticValue.AConstructor);
    }

    @Test
    void createFromConstructor_OutsideDataFile_BusinessConstructorInjection() {
        BusinessConstructorInjection businessConstructorInjection = null;
        try {
            businessConstructorInjection = (BusinessConstructorInjection) container.create(BusinessConstructorInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, businessConstructorInjection);
        assertEquals(businessConstructorInjection.toString(), StaticValue.AConstructor + "\n" + StaticValue.BConstructor + "\n" + StaticValue.AConstructor);
    }

    private Container getContainer() {
        Container container;
        try {
            String fileName = "../constructor_injection.json";
            File file = new File(getClass().getResource(fileName).toURI());
            container = new Container(file);
        } catch (IOException | ClassNotFoundException | URISyntaxException e) {
            assert false;
            return null;
        }
        return container;
    }
}
