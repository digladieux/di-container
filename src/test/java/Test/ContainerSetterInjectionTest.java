package Test;

import Container.Container;
import SetterInjection.ASetterInjection;
import SetterInjection.BusinessSetterInjection;
import SetterInjection.CSetterInjection;
import SetterInjection.ISetterInjection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ContainerSetterInjectionTest {
    private Container container = getContainer();

    @Test
    void createFromSetter_InsideDataFile_ASetterInjection() {
        ASetterInjection aSetterInjection = null;
        try {
            aSetterInjection = (ASetterInjection) container.create(ASetterInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, aSetterInjection);
        Assertions.assertEquals(aSetterInjection.toString(), StaticValue.ASetter);
    }

    @Test
    void createFromSetter_InsideDataFile_ISetterInjection() {
        ISetterInjection setterInjection = null;
        try {
            setterInjection = (ISetterInjection) container.create(ISetterInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, setterInjection);
        Assertions.assertEquals(setterInjection.toString(), StaticValue.ASetter);
    }

    @Test
    void createFromSetter_OutsideDataFile_BusinessSetterInjection() {
        BusinessSetterInjection businessSetterInjection = null;
        try {
            businessSetterInjection = (BusinessSetterInjection) container.create(BusinessSetterInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, businessSetterInjection);
        assertEquals(businessSetterInjection.toString(), StaticValue.ASetter + "\n" + StaticValue.BSetter + "\n" + StaticValue.ASetter);
    }

    @Test
    void createFromSetter_OutsideDataFile_CSetterInjection() {
        CSetterInjection cSetterInjection = null;
        try {
            cSetterInjection = (CSetterInjection) container.create(CSetterInjection.class);
        } catch (Exception e) {
            assert false;
        }

        assertNotEquals(null, cSetterInjection);
        Assertions.assertEquals(cSetterInjection.toString(), StaticValue.ASetter);
    }

    private Container getContainer() {
        Container container;
        try {
            String fileName = "../setter_injection.json";
            File file = new File(getClass().getResource(fileName).toURI());
            container = new Container(file);
        } catch (IOException | ClassNotFoundException | URISyntaxException e) {
            assert false;
            return null;
        }
        return container;
    }
}
