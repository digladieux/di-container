package FieldInjection;

import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.FIELD)
public class CFieldInjection implements IFieldInjection {

    private AFieldInjection aFieldInjection;

    public CFieldInjection() {
    }

    @Override
    public String toString() {
        return this.aFieldInjection.toString();
    }

}
