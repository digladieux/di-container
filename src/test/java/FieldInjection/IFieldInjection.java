package FieldInjection;

import Annotations.DependencyType;
import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.FIELD)
@DependencyType(AFieldInjection.class)
public interface IFieldInjection {
    @Override
    String toString() ;

}
