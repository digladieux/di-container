package FieldInjection;

import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.FIELD)
public class AFieldInjection implements IFieldInjection {

    private String value ;

    public AFieldInjection() {

    }

    @Override
    public String toString() {
        return this.value;
    }
}
