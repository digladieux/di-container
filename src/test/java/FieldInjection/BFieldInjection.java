package FieldInjection;

import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.FIELD)
public class BFieldInjection implements IFieldInjection {

    private String value ;

    public BFieldInjection() {}

    @Override
    public String toString() {
        return this.value;
    }

}
