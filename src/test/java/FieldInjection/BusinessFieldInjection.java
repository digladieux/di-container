package FieldInjection;

import Annotations.DependencyType;
import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.FIELD)
public class BusinessFieldInjection {

    @DependencyType(AFieldInjection.class)
    private IFieldInjection fieldInjectionFirst;

    @DependencyType(BFieldInjection.class)
    private IFieldInjection fieldInjectionSecond;

    private CFieldInjection cFieldInjection;

    public BusinessFieldInjection() {
    }

    @Override
    public String toString() {
        return this.fieldInjectionFirst.toString() + "\n" + this.fieldInjectionSecond.toString() + "\n" + this.cFieldInjection.toString();
    }
}
