package ErrorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class InvalidNumberPropertiesSetterInsideFile {

    private String value;

    public InvalidNumberPropertiesSetterInsideFile() {
    }

    public void setValue(String value) {
        this.value = value;
    }
}
