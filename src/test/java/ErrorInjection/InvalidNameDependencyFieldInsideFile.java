package ErrorInjection;


import Annotations.DependencyType;
import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.FIELD)
public class InvalidNameDependencyFieldInsideFile {

    @DependencyType(InvalidNameSetterOutsideFile.class)
    private InvalidNameSetterOutsideFile invalidNameSetterOutsideFile;

    public InvalidNameDependencyFieldInsideFile() {
    }

}
