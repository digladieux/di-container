package ErrorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class InvalidNameSetterOutsideFile {

    private String value;

    public InvalidNameSetterOutsideFile() {
    }

    public void setValueInvalidName(String value) {
        this.value = value;
    }
}
