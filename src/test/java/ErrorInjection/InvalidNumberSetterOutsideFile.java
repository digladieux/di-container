package ErrorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class InvalidNumberSetterOutsideFile {

    private String value;

    public InvalidNumberSetterOutsideFile() {
    }
}
