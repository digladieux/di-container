package ErrorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.CONSTRUCTOR)
public class InvalidNumberArgumentsConstructor {

    private String value;

    public InvalidNumberArgumentsConstructor() {
    }
}
