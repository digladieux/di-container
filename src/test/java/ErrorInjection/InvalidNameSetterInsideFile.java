package ErrorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class InvalidNameSetterInsideFile {

    private String value;

    public InvalidNameSetterInsideFile() {
    }

    public void setValueInvalidName(String value) {
        this.value = value;
    }
}
