package ErrorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class InvalidNumberSetterInsideFile {

    private String value;

    public InvalidNumberSetterInsideFile() {
    }
}
