package ErrorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.SETTER)
public class InvalidNumberArgumentsSetter {

    private String value;

    public InvalidNumberArgumentsSetter() {
    }

    public void setValue() {
    }
}
