package ErrorInjection;


import Annotations.InjectionTypeAnnotation;
import Annotations.InjectionTypeEnum;

@InjectionTypeAnnotation(InjectionTypeEnum.FIELD)
public class InvalidNamePropertiesFieldInsideFile {

    private String toto;

    public InvalidNamePropertiesFieldInsideFile() {
    }

}
