# TP DI-CONTAINER

## Etudiant

- Gladieux Cunha Dimitri
- Gonzales Florian

## Injections

Les injections se font de deux manières
- Un fichier de configuration pour indiquer les variables à injecter et leurs valeurs, où les classes qui font références
- De nombreuses annotations
    - `@SingleInstance` : On souhaite une instance unique de l'objet. Si non présente, on cré un nouvel objet
    - `@DependencyType` : Indique la classe que l'on souhaite injecter. Par exemple pour IBank, on met @DependencyType(BNP.class) au dessus du setter, field, class suivant le type du container
    - `@InjectionTypeAnnotation` : Indique si on injecte par Setter, Constructeur ou Field

## Test

Un dossier de test est présent pour tester 
- Le parser (Json file)
- Le modèle Json à un modèle que je peux utiliser
- Les 3 types de container (setter, constructor, field)
- Toutes les erreurs possibles de l'utilisateur
On arrive sur un coverage de 100%

## Objectifs Réalisés

- **US0 : Objects and values** - Prenez n'importe quelle classe, on peut injecter des valeurs ou des classes
- **US1 : Setter injection** - ContainerSetterInjection
- **US2 : Constructor injection** - ContainerConstructorInjection
- **US3 : Field injection** - ContainerFieldInjection
- **US4 : Dependency Resolution (binding)** - Le fichier de configuration avec la propriété `refs`
- **US5 : Support Singleton** - L'annotation @SingleInstance au dessus de la classe permet de réaliser cela
- **US6 : Multiple Implementations** - L'annotation @DependencyType est la pour déterminer le choix de l'injection. Regarder les classes `Business` où j'utilise une interface implémenter par 2 classes différentes
- **US7 : Autowiring** - Par default, j'implemente par field
- **US10 : Graph Resolution** - Les classes Business reprennent ce shéma. Class Business -> Interface I -> Class Injection -> String value
- **US9 : Lazy initialisation** - Je suis pas sur d'avoir compris la question ..

## Contraintes

Maven, Java 8 et tests unitaires utilisés
